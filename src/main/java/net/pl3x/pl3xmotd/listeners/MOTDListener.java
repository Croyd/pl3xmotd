package net.pl3x.pl3xmotd.listeners;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;
import java.util.Random;

import net.pl3x.pl3xmotd.Pl3xMOTD;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.util.CachedServerIcon;

public class MOTDListener implements Listener {
	private Pl3xMOTD plugin;

	public MOTDListener(Pl3xMOTD plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onPing(final ServerListPingEvent event) {
		List<String> motd = plugin.getConfig().getStringList("server-motd");
		if (motd.size() == 0)
			return;
		int motdchoice = new Random().nextInt(motd.size());
		event.setMotd(plugin.colorize(motd.get(motdchoice).replace("\\n", "\n")));
		File dir =  new File(plugin.getDataFolder() + File.separator + "server-icon");
		if (!dir.exists())
			return;
		File[] iconFiles = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".png");
			}
		});
		if (iconFiles.length < 1)
			return;
		int iconchoice = new Random().nextInt(iconFiles.length);
		CachedServerIcon icon = null;
		try {
			icon = plugin.getServer().loadServerIcon(iconFiles[iconchoice]);
		} catch (Exception e) {
			plugin.log("&4[ERROR] Problem loading custom server-icon: &7" + e.getLocalizedMessage());
		}
		if (icon == null)
			return;
		event.setServerIcon(icon);
	}
}

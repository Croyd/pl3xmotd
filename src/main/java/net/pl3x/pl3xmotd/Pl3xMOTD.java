package net.pl3x.pl3xmotd;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import net.pl3x.pl3xmotd.commands.CmdPl3xMOTD;
import net.pl3x.pl3xmotd.listeners.MOTDListener;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class Pl3xMOTD extends JavaPlugin {
	public void onEnable() {
		if (!new File(getDataFolder() + File.separator + "config.yml").exists())
			saveDefaultConfig();

		Bukkit.getPluginManager().registerEvents(new MOTDListener(this), this);

		getCommand("pl3xmotd").setExecutor(new CmdPl3xMOTD(this));

		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}

		log(getName() + " v" + getDescription().getVersion() + " by BillyGalbreath enabled!");
	}

	public void onDisable() {
		log(getName() + " Disabled.");
	}

	public void log(Object obj) {
		if (getConfig().getBoolean("color-logs", true))
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" + getName() + "&3]&r " + obj));
		else
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
	}

	public String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
}
